package model.vo;

import model.data_structures.LinkedList;

public class ServiciosValorPagado implements Comparable<ServiciosValorPagado>{
	
	private LinkedList<Servicio> serviciosAsociados;
	private double valorAcumulado;
	public LinkedList<Servicio> getServiciosAsociados() {
		return serviciosAsociados;
	}
public ServiciosValorPagado(LinkedList<Servicio> pServiciosAsociados,double pValorAcumulado ) {
		
		serviciosAsociados=pServiciosAsociados;
		valorAcumulado=pValorAcumulado;
	}
	public void setServiciosAsociados(LinkedList<Servicio> serviciosAsociados) {
		this.serviciosAsociados = serviciosAsociados;
	}
	public double getValorAcumulado() {
		return valorAcumulado;
	}
	public void setValorAcumulado(double valorAcumulado) {
		this.valorAcumulado = valorAcumulado;
	}
	@Override
	public int compareTo(ServiciosValorPagado arg0) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	

}
