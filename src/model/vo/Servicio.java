package model.vo;

import java.util.Date;

/**
 * Representation of a Service object
 */
public class Servicio implements Comparable<Servicio>{	
	private String taxiId;
	private String tripId;
	private int tripSecons;
	private double tripMiles;
	private Date fechaInicio;
	private Date fechaFinal;
	private Taxi taxiServicio;
	private double trp_total;
	private String id_zonaInicial;
	private String id_zonaFinal;
	
	
	
	public Servicio(String pTaxiId,String pTripId,int pTripSecons,double pTripMiles, Date pFechaInicio, Date pFechaFinal, Taxi pTaxiServicio , double pTripTotal, String idZonaI, String idZOnaF) {
		taxiId =pTaxiId;
		tripId=pTripId;
		tripSecons= pTripSecons;
		tripMiles= pTripMiles;
		fechaInicio=(pFechaInicio);
		fechaFinal=(pFechaFinal);
		taxiServicio= pTaxiServicio;
		trp_total=(pTripTotal);
		id_zonaInicial= idZonaI;
		id_zonaFinal=idZOnaF;
	}
	/**
	 * @return id - Trip_id
	 */
	public String getTripId()
	{
		// TODO Auto-generated method stub
		return tripId;
		
	}	
	
	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
	
		return taxiId;
		
	}	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		// TODO Auto-generated method stub
		return tripSecons;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		// TODO Auto-generated method stub
		return tripMiles;
		}

	
	@Override
	public int compareTo(Servicio arg0) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	
	public Date getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public Date getFechaFinal() {
		return fechaFinal;
	}
	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}
	public Taxi getTaxiServicio() {
		return taxiServicio;
	}
	public void setTaxiServicio(Taxi taxiServicio) {
		this.taxiServicio = taxiServicio;
	}
	public double getTrp_total() {
		return trp_total;
	}
	public void setTrp_total(double trp_total) {
		this.trp_total = trp_total;
	}
	public String getId_zonaInicial() {
		return id_zonaInicial;
	}
	public void setId_zonaInicial(String id_zonaInicial) {
		this.id_zonaInicial = id_zonaInicial;
	}
	public String getId_zonaFinal() {
		return id_zonaFinal;
	}
	public void setId_zonaFinal(String id_zonaFinal) {
		this.id_zonaFinal = id_zonaFinal;
	}
	
}